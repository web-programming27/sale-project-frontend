export default interface User {
  id?: number;
  name: string;
  login: string;
  password: string;
  address: string;
  tel: string;
  email: string;
  position: string;
  hourlywage: number;

  createdAt?: Date;

  updatedAt?: Date;

  deletedAt?: Date;
}
