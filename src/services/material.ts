import type Material from "@/types/Material";
import http from "./axios";
function getMaterials(params: any) {
  return http.get("/materials", { params: params });
}
function saveMaterial(materials: {
  name: string;
  min_quantity: number;
  quantity: number;
  unit: string;
  price_per_unit: number;
}) {
  return http.post("/materials", materials);
}
function updateMaterial(id: number, materials: Material) {
  return http.patch(`/materials/${id}`, materials);
}
function deleteMaterial(id: number) {
  return http.delete(`/materials/${id}`);
}
export default { getMaterials, saveMaterial, updateMaterial, deleteMaterial };
