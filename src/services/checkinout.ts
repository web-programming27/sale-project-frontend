import type Checkinout from "@/types/CheckMaterial";
import http from "./axios";
function getCheckinouts(params: any) {
  return http.get("/checkinout", { params: params });
}

function saveCheckinouts(checkinout: { employeeId: number }) {
  return http.post("/checkinout", checkinout);
}
function updateCheckinout(id: number) {
  return http.patch(`/checkinout/${id}`);
}
function daleteCheckinout(id: number) {
  return http.delete(`/checkinout/${id}`);
}
export default {
  getCheckinouts,
  saveCheckinouts,
  updateCheckinout,
  daleteCheckinout,
};
