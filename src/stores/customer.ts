import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Customer from "@/types/Customer";
import customerService from "@/services/customer";
import reportService from "@/services/report";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import type CustomerByTel from "@/types/CustomerByTel";
import { useRecieptStore } from "./reciept";
import { useMessageSuccessStore } from "./messageSuccess";
import type CustomerByPoint from "@/types/ReportCustomerByPoint";
import type ReportCustomerByTel from "@/types/ReportCustomerByTel";

export const useCustomerStore = defineStore("Customer", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const messageSuccessStore = useMessageSuccessStore();
  const dialog = ref(false);
  const Teldialog = ref(false);
  const customers = ref<Customer[]>([]);

  const nameMember = ref("");
  const pointMember = ref(0);
  const IDMember = ref(0);
  const TelMember = ref("");
  const pluspoint = ref(0);

  const POSnameMember = ref("");
  const POSpointMember = ref(0);
  const POSIDMember = ref(0);
  const POSTelMember = ref("");

  const tel = ref("");
  const customerBypoint = ref<CustomerByPoint[]>([]);
  const customerBytel = ref<Customer[]>([]);
  const editedCustomerByPoint = ref<CustomerByPoint>({
    customer_name: "",
    customer_surname: "",
    point: 0,
  });
  const editedCustomer = ref<Customer>({
    name: "",
    tel: "",
    point: 0,
  });

  watch(tel, async (newTel, oldTel) => {
    await getCustomerByCallStored();
  });
  const recieptStore = useRecieptStore();

  function Cusdiscount() {
    recieptStore.discount = 10;
  }

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedCustomer.value = {
        name: "",
        tel: "",
        point: 0,
      };
    }
  });

  const findCustomer = ref<CustomerByTel>({
    tel: "",
  });

  watch(Teldialog, (newTelDialog, oldTeldialog) => {
    console.log(newTelDialog);
    if (!newTelDialog) {
      findCustomer.value = {
        tel: "",
      };
    }
  });

  async function getCustomerByPoint() {
    try {
      const res = await reportService.getCustomeView();
      customerBypoint.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่มีข้อมูล Customer");
    }
  }

  async function getCustomersByTel() {
    loadingStore.isLoading = true;
    try {
      const res = await customerService.getCustomersByTel(
        findCustomer.value.tel
      );
      customers.value = res.data;
      // recieptStore.discount = editedCustomer.value.point;
      // console.log(recieptStore.discount);
      messageSuccessStore.showInfo("เจอข้อมูล Customer แล้ว");
      Teldialog.value = false;
      nameMember.value = res.data.name;
      IDMember.value = res.data.id;
      TelMember.value = res.data.tel;
      pointMember.value = res.data.point;
      // Cusdiscount();
      // recieptStore.plusPoint();
      editedCustomer.value = {
        name: nameMember.value,
        tel: TelMember.value,
        point: pointMember.value,
      };

      customerService.updateCustomer(IDMember.value, editedCustomer.value);
      // customerService.patchCustomerPoint(IDMember.value, pointMember.value);
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่มีข้อมูล Customer");
    }
    loadingStore.isLoading = false;
  }

  async function getCustomersByTelPOS() {
    loadingStore.isLoading = true;
    try {
      const res = await customerService.getCustomersByTel(
        findCustomer.value.tel
      );
      customers.value = res.data;
      console.log(customers.value);
      messageSuccessStore.showInfo("เจอข้อมูล Customer แล้ว");
      Teldialog.value = false;

      POSnameMember.value = res.data.name;
      POSIDMember.value = res.data.id;
      POSTelMember.value = res.data.tel;
      POSpointMember.value = res.data.point;
      console.log(POSpointMember.value);
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่มีข้อมูล Customer");
    }
    loadingStore.isLoading = false;
  }

  // async function getCustomers() {
  //   loadingStore.isLoading = true;
  //   try {
  //     const res = await customerService.getCustomers({});
  //     customers.value = res.data;
  //   } catch (e) {
  //     console.log(e);
  //     messageStore.showError("ไม่สามารถดึงข้อมูล Customer ได้");
  //   }
  //   loadingStore.isLoading = false;
  // }
  const getCustomerByCallStored = async () => {
    const res = await reportService.getReportByTel({
      searchTel: tel.value,
    });
    customerBytel.value = res.data;
  };
  async function getCustomers() {
    loadingStore.isLoading = true;
    try {
      const res = await reportService.getCustomeView();
      customers.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Customer ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveCustomer() {
    loadingStore.isLoading = true;
    try {
      if (editedCustomer.value.id) {
        const res = await customerService.updateCustomer(
          editedCustomer.value.id,
          editedCustomer.value
        );
      } else {
        const res = await customerService.saveCustomer(editedCustomer.value);
      }

      dialog.value = false;

      await getCustomers();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Customer ได้");
      console.log(e);
    }
    window.location.reload();
    loadingStore.isLoading = false;
  }
  async function deleteCustomer(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await customerService.daleteCustomer(id);
      await getCustomers();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Customer ได้");
    }
    window.location.reload();
    loadingStore.isLoading = false;
  }

  function editCustomer(customer: Customer) {
    editedCustomer.value = JSON.parse(JSON.stringify(customer));
    dialog.value = true;
  }
  return {
    customers,
    getCustomers,
    dialog,
    Teldialog,
    editedCustomer,
    saveCustomer,
    editCustomer,
    deleteCustomer,
    getCustomersByTel,
    findCustomer,
    nameMember,
    pointMember,
    IDMember,
    pluspoint,
    customerBypoint,
    getCustomerByPoint,
    editedCustomerByPoint,
    Cusdiscount,
    tel,
    getCustomerByCallStored,
    customerBytel,
    getCustomersByTelPOS,
    TelMember,
    POSnameMember,
    POSIDMember,
    POSTelMember,
    POSpointMember,
  };
});
