import type Customer from "@/types/Customer";
import http from "./axios";
function getCustomers(params: any) {
  return http.get("/customers", { params: params });
}
function getCustomersByTel(tel: string) {
  return http.get(`/customers/tel/${tel}`);
}
function saveCustomer(customers: Customer) {
  return http.post("/customers", customers);
}
function updateCustomer(id: number, customers: Customer) {
  return http.patch(`/customers/${id}`, customers);
}
function daleteCustomer(id: number) {
  return http.delete(`/customers/${id}`);
}

function updateCustomerPoint(id: number, point: number) {
  return http.patch(`/customers/${id}/point`, point);
}
export default {
  getCustomers,
  saveCustomer,
  updateCustomer,
  daleteCustomer,
  getCustomersByTel,
  updateCustomerPoint,
};
