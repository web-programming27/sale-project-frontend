export default interface ReportCustomerByTel {
  id?: number;
  customer_name: string;
  customer_surname: string;
  point: number;
}
