import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type User from "@/types/User";
import { useLoadingStore } from "./loading";
import userService from "@/services/user";
import { useMessageStore } from "./message";

export const useUserPageStore = defineStore("userPage", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const page = ref(1);
  const take = ref(10);
  const keyword = ref("");
  const users = ref<User[]>([]);
  const lastPage = ref(0);
  watch(keyword, async (newKeyword, oldKeyword) => {
    await getUsers();
  });
  watch(page, async (newPage, oldPage) => {
    await getUsers();
  });
  watch(lastPage, async (newlastPage, oldlastPage) => {
    if (newlastPage < page.value) {
      page.value = 1;
    }
  });
  async function getUsers() {
    loadingStore.isLoading = true;
    try {
      const res = await userService.getUsers({
        page: page.value,
        take: take.value,
        keyword: keyword.value,
      });
      users.value = res.data.data;
      lastPage.value = res.data.lastPage;
      console.log(users.value);
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Employee ได้");
    }
    loadingStore.isLoading = false;
  }

  return { page, take, keyword, users, getUsers, lastPage };
});
