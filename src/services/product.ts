import type Product from "@/types/Product";
import http from "./axios";
function getProducts() {
  return http.get("/products");
}
function getProductsByCatagory(catagory: number) {
  return http.get(`/products/catagory/${catagory}`);
}

function saveProduct(products: Product & { files: File[] }) {
  const formData = new FormData();
  formData.append("name", products.name);
  formData.append("price", "" + products.price);
  formData.append("size", "" + products.size);
  formData.append("size_unit", products.size_unit);
  // formData.append("catagoryId", "" + products.catagoryId);
  formData.append("catagoryId", `${products.catagoryId}`);
  formData.append("file", products.files[0]);
  return http.post("/products", formData, {
    headers: { "Content-type": "multipart/form-data" },
  });
}
function updateProduct(id: number, products: Product & { files: File[] }) {
  const formData = new FormData();
  formData.append("name", products.name);
  formData.append("price", "" + products.price);
  formData.append("size", "" + products.size);
  formData.append("size_unit", products.size_unit);
  formData.append("catagoryId", `${products.catagoryId}`);
  if (products.files.length > 0) {
    formData.append("file", products.files[0]);
  }
  return http.patch(`/products/${id}`, formData, {
    headers: { "Content-type": "multipart/form-data" },
  });
}
function daleteProduct(id: number) {
  return http.delete(`/products/${id}`);
}

export default {
  getProducts,
  saveProduct,
  updateProduct,
  daleteProduct,
  getProductsByCatagory,
};
