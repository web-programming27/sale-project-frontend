import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Material from "@/types/Material";
import materialService from "@/services/material";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import { useCheckMaterialDetailStore } from "./checkmaterialdetail";

export const useMaterialStore = defineStore("Material", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const materials = ref<Material[]>([]);
  const editedMaterial = ref<Material>({
    name: "",
    min_quantity: 0,
    quantity: 0,
    unit: "",
    price_per_unit: 0,
    qty_remain: 0,
    qty_expire: 0,
  });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedMaterial.value = {
        name: "",
        min_quantity: 0,
        quantity: 0,
        unit: "",
        price_per_unit: 0,
        qty_remain: 0,
        qty_expire: 0,
      };
    }
  });

  async function getMaterials() {
    loadingStore.isLoading = true;
    try {
      const res = await materialService.getMaterials({});
      materials.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Material ได้");
    }
    loadingStore.isLoading = false;
  }
  async function saveMaterial() {
    loadingStore.isLoading = true;
    try {
      if (editedMaterial.value.id) {
        const res = await materialService.updateMaterial(
          editedMaterial.value.id,
          editedMaterial.value
        );
      } else {
        const res = await materialService.saveMaterial(editedMaterial.value);
      }

      dialog.value = false;

      await getMaterials();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Material ได้");
      console.log(e);
    }
    window.location.reload();
    loadingStore.isLoading = false;
  }
  async function deleteMaterial(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await materialService.deleteMaterial(id);
      await getMaterials();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Material ได้");
    }
    window.location.reload();
    loadingStore.isLoading = false;
  }

  function editMaterial(material: Material) {
    editedMaterial.value = JSON.parse(JSON.stringify(material));
    dialog.value = true;
    console.log(material);
  }
  return {
    materials,
    getMaterials,
    dialog,
    editedMaterial,
    saveMaterial,
    editMaterial,
    deleteMaterial,
  };
});
