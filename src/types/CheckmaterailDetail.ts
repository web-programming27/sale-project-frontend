import type CheckMaterial from "./CheckMaterial";
import type Material from "./Material";

export default interface Checkmaterialdetail {
  id?: number;
  name?: string;
  qty_last?: number;
  qty_remain: number;
  qty_expire: number;
  material?: Material;
  checkmaterial?: CheckMaterial;
}
