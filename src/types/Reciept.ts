import type Customer from "./Customer";
import type User from "./User";
import type Store from "./Store";
import type RecieptDetail from "./RecieptDetail";
export default interface Reciept {
  id?: number;

  date?: Date;

  discount?: number;

  total?: number;

  received?: number;

  change?: number;

  payment?: string;

  amount?: number;

  updatedDate?: Date;

  deletedDate?: Date;

  recieptDetails?: RecieptDetail[];

  customer?: Customer;

  store?: Store;

  employee?: User;

  show?: boolean;
}
