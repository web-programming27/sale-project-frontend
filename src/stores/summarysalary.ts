import { defineStore } from "pinia";
import { useLoadingStore } from "./loading";
import summarysalaryService from "@/services/summarysalary";
import { useMessageSuccessStore } from "./messageSuccess";
import { useMessageStore } from "./message";
import type SummarySalary from "@/types/SummarySalary";
import { ref } from "vue";
const loadingStore = useLoadingStore();
const messageStore = useMessageStore();
const messageSuccessStore = useMessageSuccessStore();
const paysalarydialog = ref(false);
const summarysalary = ref<SummarySalary[]>([]);
// const editedSalary = ref<SummarySalary>({
//   ss_date: "",
//   ss_salary: "",
//   ss_work_hour: "",
// });
// const paysalarydialog = ref(false);
export const useSummarysalaryStore = defineStore("Summarysalary", () => {
  async function getSummarysalary() {
    loadingStore.isLoading = true;
    try {
      const res = await summarysalaryService.getSummarysalary();
      summarysalary.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่มีข้อมูล Summarysalary");
    }
    loadingStore.isLoading = false;
  }

  async function deleteSummarySalary(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await summarysalaryService.daleteSummarysalary(id);
      messageSuccessStore.showConfirm("จ่ายเงินเดือนสำเร็จ");
      await getSummarysalary();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถจ่ายเงินเดือนได้");
    }
    loadingStore.isLoading = false;
    window.location.reload();
  }
  return {
    summarysalary,
    getSummarysalary,
    paysalarydialog,
    deleteSummarySalary,
  };
});
