import http from "./axios";
function getCustomeView() {
  return http.get("/reports/view/customer");
}
function getReportByTel(params: any) {
  return http.get(`/reports/customer`, { params: params });
}

function getBestOrderChart() {
  return http.get("/reports/recieptdetail/bestseller/chart");
}

function getBestEmployee() {
  return http.get("/reports/view/employee/sale");
}

function getWorstOrderChart() {
  return http.get("/reports/recieptdetail/worstseller/chart");
}

function getCusByMonthChart() {
  return http.get("/reports/customer/bymonth/chart");
}

function getSaleChart() {
  return http.get("/reports/reciept/sale");
}
function sumOrder() {
  return http.get("/reports/sumorder");
}

function sumSale() {
  return http.get("/reports/sumsale");
}
export default {
  getCustomeView,
  getReportByTel,
  getBestOrderChart,
  getWorstOrderChart,
  getCusByMonthChart,
  getSaleChart,
  getBestEmployee,
  sumOrder,
  sumSale,
};
