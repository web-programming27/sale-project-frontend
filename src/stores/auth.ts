import { ref, computed } from "vue";
import { defineStore } from "pinia";
import auth from "@/services/auth";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import router from "@/router";
import type User from "@/types/User";
import { useCheckinoutStore } from "./checkinout";
export const useAuthStore = defineStore("auth", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const authName = ref("");
  const checkinoutStore = useCheckinoutStore();
  const getUser = () => {
    const userString = localStorage.getItem("user");
    if (!userString) return null;
    const user = JSON.parse(userString ?? "");
    return user;
  };

  const isLogin = () => {
    const user = localStorage.getItem("user");
    if (user) {
      return true;
    }
    return false;
  };
  const login = async (username: string, password: string): Promise<void> => {
    loadingStore.isLoading = true;
    try {
      const res = await auth.login(username, password);
      console.log(res.data);
      localStorage.setItem("user", JSON.stringify(res.data.user));
      localStorage.setItem("token", res.data.access_token);
      localStorage.setItem("name", res.data.user.name);
      localStorage.setItem("position", res.data.user.position);

      router.push("/home");
    } catch (e) {
      messageStore.showError("Username หรือ Password ไม่ถูกต้อง");
    }
    loadingStore.isLoading = false;
    //localStorage.setItem("token", userName);
  };
  const logout = () => {
    //authName.value = "";
    localStorage.removeItem("user");
    localStorage.removeItem("token");
    router.replace("/");
  };

  return { login, logout, isLogin, getUser };
});
