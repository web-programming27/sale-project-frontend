export default interface CustomerByPoint {
  id?: number;
  customer_name: string;
  customer_surname: string;
  point: number;
}
