import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Employee from "@/types/User";
import { useLoadingStore } from "./loading";
import employeeService from "@/services/employee";
import { useMessageStore } from "./message";

export const useEmployeePageStore = defineStore("employeePage", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const page = ref(1);
  const take = ref(10);
  const keyword = ref("");
  const employees = ref<Employee[]>([]);
  const lastPage = ref(0);
  watch(keyword, async (newKeyword, oldKeyword) => {
    await getEmployees();
  });
  watch(page, async (newPage, oldPage) => {
    await getEmployees();
  });
  watch(lastPage, async (newlastPage, oldlastPage) => {
    if (newlastPage < page.value) {
      page.value = 1;
    }
  });
  async function getEmployees() {
    loadingStore.isLoading = true;
    try {
      const res = await employeeService.getEmployees({
        page: page.value,
        take: take.value,
        keyword: keyword.value,
      });
      employees.value = res.data.data;
      lastPage.value = res.data.lastPage;
      console.log(employees.value);
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Employee ได้");
    }
    loadingStore.isLoading = false;
  }

  return { page, take, keyword, employees, getEmployees, lastPage };
});
