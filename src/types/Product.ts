export default interface Product {
  id?: number;
  name: string;
  size: number;
  size_unit: string;
  price: number;
  qty: number;
  image?: string;
  catagoryId: number;
  createdAt?: Date;

  updatedAt?: Date;

  deletedAt?: Date;
}
