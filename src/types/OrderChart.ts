export default interface OrderChart {
  name: string;
  bestseller: number;
}
