import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Reciept from "@/types/Reciept";
import recieptService from "@/services/reciept";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import type Product from "@/types/Product";
import { useAuthStore } from "./auth";
import { useCustomerStore } from "./customer";
import customerService from "@/services/customer";
import { useMessageSuccessStore } from "./messageSuccess";
import reportService from "@/services/report";

export const useRecieptStore = defineStore("Reciept", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const authStore = useAuthStore();
  const customerStore = useCustomerStore();
  const dialog = ref(false);
  const reciepts = ref<Reciept[]>([]);
  const editedReciept = ref<Reciept>({});
  const messageSuccessStore = useMessageSuccessStore();
  const received = ref(0);
  const discount = ref(0);
  const total = ref(0);
  const Billdialog = ref(false);
  const promotion = ref(false);
  const QRcodeDialog = ref(false);
  const change = ref(0);
  const recieptStore = useRecieptStore();

  const customerId = ref(0);
  const customerName = ref("");
  const customerTel = ref("");
  const customerPoint = ref(0);
  const disabledIn = ref(false);

  const sumorder = ref<string[]>([]);
  const sum_order = ref<string>("");

  const sumsale = ref<string[]>([]);
  const sum_sale = ref<string>("");

  async function getSumOrder() {
    try {
      const res = await reportService.sumOrder();
      sumorder.value = [res.data[0].total_receipt_amount.toString()];
      console.log("sumorder", sumorder);
      sum_order.value = sumorder.value[0].replace(/[{},"]/g, "");
      console.log("sum_oder", sum_order.value);
    } catch (e) {
      console.log(e);
    }
  }

  async function getSumSales() {
    try {
      const res = await reportService.sumSale();
      sumsale.value = [res.data[0].sumsale.toString()]; // เข้าถึงค่าใน properties และใช้ Object.values() เพื่อดึงค่าออกมา
      console.log("sumsale", sumsale);
      sum_sale.value = sumsale.value[0].replace(/[{},"]/g, "");
      console.log("sum_sale", sum_sale.value);
    } catch (e) {
      console.log(e);
    }
  }

  function promotion1() {
    if (customerStore.POSpointMember >= 10) {
      customerStore.POSpointMember -= 10;
      discount.value = 10;
      disabledIn.value = true;
      recieptStore.promotion = false;
    } else {
      messageStore.showError("PointCustomer ไม่พอ");
      recieptStore.promotion = true;
    }
    console.log("pointMember", customerStore.POSpointMember);
    console.log("discount", discount.value);
  }

  function promotion2() {
    if (customerStore.POSpointMember >= 20) {
      customerStore.POSpointMember -= 20;
      discount.value = 20;
      disabledIn.value = true;
      recieptStore.promotion = false;
    } else {
      messageStore.showError("PointCustomer ไม่พอ");
      recieptStore.promotion = true;
    }
    console.log("pointMember", customerStore.POSpointMember);
    console.log("discount", discount.value);
  }
  function promotion3() {
    if (customerStore.POSpointMember >= 30) {
      customerStore.POSpointMember -= 30;
      discount.value = 45;
      disabledIn.value = true;
      recieptStore.promotion = false;
    } else {
      messageStore.showError("PointCustomer ไม่พอ");
      recieptStore.promotion = true;
    }
    console.log("pointMember", customerStore.POSpointMember);
    console.log("discount", discount.value);
  }
  function clearReciept() {
    orderList.value = [];
    received.value = 0;
    customerStore.pointMember = 0;
    payment.value = "";
    discount.value = 0;
    customerStore.POSnameMember = "";
    customerStore.POSpointMember = 0;
  }
  const payment = ref("");

  function cash() {
    return (payment.value = "Cash");
  }
  function wallet() {
    return (payment.value = "E-Wallet");
  }
  function card() {
    return (payment.value = "Card");
  }

  function plusPoint() {
    if (sumPrice.value >= 50) {
      customerStore.POSpointMember += 5;
    }
  }

  const orderList = ref<
    { product: Product; amount: number; sum: number; image?: string }[]
  >([]);

  function addProduct(item: Product) {
    for (let i = 0; i < orderList.value.length; i++) {
      if (orderList.value[i].product.id === item.id) {
        orderList.value[i].amount++;
        orderList.value[i].sum = orderList.value[i].amount * item.price;
        orderList.value[i].image = item.image;
        return;
      }
    }
    orderList.value.push({
      product: item,
      amount: 1,
      sum: 1 * item.price,
      image: item.image,
    });
  }

  function deleteProductList(index: number) {
    orderList.value.splice(index, 1);
  }
  const sumPrice = computed(() => {
    let sum = 0;
    for (let i = 0; i < orderList.value.length; i++) {
      sum = sum + orderList.value[i].sum;
    }
    return sum;
  });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedReciept.value = {};
    }
  });

  // watch(Billdialog, (newBilldialog, oldBilldialog) => {
  //   console.log(newBilldialog);
  //   if (change.value < 0) {
  //     Billdialog.value = false;
  //     messageStore.showError("ยังไม่ได้รับเงินมาครบจำนวน");
  //   }
  //   if (!newBilldialog) {
  //     discount.value;
  //     total.value;
  //     received.value;
  //     change.value;
  //     payment.value;
  //   }
  // });
  watch(Billdialog, (newBilldialog, oldBilldialog) => {
    console.log(newBilldialog);
    if (!newBilldialog) {
      discount.value;
      total.value;
      received.value;
      change.value;
      payment.value;
    }
  });

  async function openBill() {
    if (change.value < 0) {
      messageStore.showError("ยังไม่ได้รับเงินมาครบจำนวน");
    } else {
      Billdialog.value = true;
    }
  }

  async function getReciepts() {
    loadingStore.isLoading = true;
    try {
      const res = await recieptService.getReciepts();
      reciepts.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Reciept ได้");
    }
    loadingStore.isLoading = false;
  }

  async function openReciept() {
    loadingStore.isLoading = true;
    const user: { id: number } = authStore.getUser();
    const recieptDetails = orderList.value.map(
      (item) =>
        <{ productId: number; amount: number }>{
          productId: item.product.id,
          amount: item.amount,
        }
    );
    const reciept = {
      customerId: customerStore.POSIDMember,
      storeId: 1,
      employeeId: user.id,
      discount: discount.value,
      received: received.value,
      recieptDetails: recieptDetails,
      payment: payment.value,
    };
    console.log(reciept);
    if (change.value < 0) {
      messageStore.showError("ยังไม่ได้รับเงินมาครบจำนวน");
      loadingStore.isLoading = false;
    } else {
      try {
        const res = await recieptService.saveReciept(reciept);
        plusPoint();
        customerId.value = customerStore.POSIDMember;
        customerName.value = customerStore.POSnameMember;
        customerTel.value = customerStore.POSTelMember;
        customerPoint.value = customerStore.POSpointMember;

        console.log("customerId", customerId);
        console.log("customerPoint", customerPoint.value);
        // const res2 = await customerService.updateCustomerPoint(
        //   customerId.value,
        //   customerPoint.value
        // );
        if (customerId.value > 0) {
          const Customer = {
            name: customerName.value,
            tel: customerTel.value,
            point: customerPoint.value,
          };
          const res2 = await customerService.updateCustomer(
            customerId.value,
            Customer
          );
        }
        dialog.value = false;

        clearReciept();
        messageSuccessStore.showInfo("การขายสำเร็จ");
        // await getReciepts();
      } catch (e) {
        messageStore.showError("ไม่สามารถบันทึก Reciept ได้");
        console.log(e);
      }
      loadingStore.isLoading = false;
    }
  }

  // async function saveReciept() {
  //   loadingStore.isLoading = true;
  //   try {
  //     if (editedReciept.value.id) {
  //       const res = await recieptService.updateReciept(
  //         editedReciept.value.id,
  //         editedReciept.value
  //       );
  //     } else {
  //       const res = await recieptService.saveReciept(editedReciept.value);
  //     }

  //     dialog.value = false;

  //     await getReciepts();
  //   } catch (e) {
  //     messageStore.showError("ไม่สามารถบันทึก Reciept ได้");
  //     console.log(e);
  //   }
  //   loadingStore.isLoading = false;
  // }

  async function deleteReciept(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await recieptService.deleteReciept(id);
      await getReciepts();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Reciept ได้");
    }
    loadingStore.isLoading = false;
  }

  function editReciept(reciept: Reciept) {
    editedReciept.value = JSON.parse(JSON.stringify(reciept));
    dialog.value = true;
  }
  return {
    reciepts,
    getReciepts,
    dialog,
    editedReciept,
    // saveReciept,
    editReciept,
    deleteReciept,
    addProduct,
    deleteProductList,
    sumPrice,
    orderList,
    openReciept,
    cash,
    wallet,
    card,
    payment,
    received,
    discount,
    total,
    Billdialog,
    openBill,
    plusPoint,
    QRcodeDialog,
    change,
    promotion,
    promotion1,
    promotion2,
    promotion3,
    disabledIn,
    sumorder,
    getSumOrder,
    sum_order,
    getSumSales,
    sum_sale,
    sumsale,
  };
});
