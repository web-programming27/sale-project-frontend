import { ref, watch } from "vue";
import { defineStore } from "pinia";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import type CheckMaterial from "@/types/CheckMaterial";
import CheckmaterialService from "@/services/checkmaterial";
import { useCheckMaterialDetailStore } from "./checkmaterialdetail";
import employeeService from "@/services/employee";
import checkmaterialservice from "@/services/checkmaterial";
import { useMessageSuccessStore } from "./messageSuccess";
import { useBillStore } from "./bill";
import type Material from "@/types/Material";
import type EmployeeByTel from "@/types/EmployeeByTel";
import { useMaterialStore } from "./material";
import { useAuthStore } from "./auth";
import { useMaterialPageStore } from "./material-page";

export const useCheckMaterialStore = defineStore("Checkmaterial", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const messageSuccessStore = useMessageSuccessStore();
  const materialStore = useMaterialStore();
  const dialog = ref(false);
  const Adddialog = ref(false);
  const employee = ref("");
  const employeeId = ref(0);
  const Teldialog = ref(false);
  const materialPageStore = useMaterialPageStore();
  const authStore = useAuthStore();
  const employeeName = ref("");
  const checkmaterialStore = useCheckMaterialStore();
  const checkmaterialdetailStore = useCheckMaterialDetailStore();
  const billStore = useBillStore();
  const checkmaterials = ref<CheckMaterial[]>([]);
  const editedCheckMaterial = ref<CheckMaterial>({
    employeeId: 0,
  });
  const findEmployee = ref<EmployeeByTel>({
    tel: "",
  });

  const material = ref<Material>();
  const checkMaterialList = ref<Material[]>([]);

  const CheckmaterialList = ref<
    { material: Material; qty_remain: number; qty_expire: number }[]
  >([]);

  async function openCheckMaterial() {
    const user: { id: number } = authStore.getUser();
    console.log(materialPageStore.materials);
    const checkmaterialDetails = materialPageStore.materials.map(
      (item) =>
        <
          {
            materialId: number;
            qty_remain: number;
            qty_expire: number;
          }
        >{
          materialId: item.id,
          qty_remain: item.qty_remain,
          qty_expire: item.qty_expire,
        }
    );

    const checkmaterial = {
      employeeId: checkmaterialStore.employeeId,
      checkmaterialDetails: checkmaterialDetails,
    };
    loadingStore.isLoading = true;
    try {
      console.log(checkmaterial);
      const res = await checkmaterialservice.saveCheckMaterial(checkmaterial);
      messageSuccessStore.showInfo("การ Check สำเร็จ");
      dialog.value = false;
      checkmaterialdetailStore.dialog = false;
      await materialStore.getMaterials();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถบันทึก checkMaterial ได้");
    }
    // await materialStore.getMaterials();
    loadingStore.isLoading = false;
  }

  function clearCheckMat() {
    employee.value = "";
    employeeId.value = 0;
    employeeName.value = "";
  }

  function clearCheckMatDetail() {
    checkmaterialdetailStore.editedCheckmaterialDetail.name = "";
    materialStore.editedMaterial.name = "";
    checkmaterialdetailStore.editedCheckmaterialDetail.qty_expire = 0;
    materialStore.editedMaterial.quantity = 0;
    checkmaterialdetailStore.editedCheckmaterialDetail.qty_remain = 0;
    materialStore.editedMaterial.id = 0;
  }

  watch(Teldialog, (newTelDialog, oldTeldialog) => {
    console.log(newTelDialog);
    if (!newTelDialog) {
      findEmployee.value = {
        tel: "",
      };
    }
  });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedCheckMaterial.value = {
        employeeId: 0,
      };
    }
  });
  watch(Adddialog, (newAdddialog, oldAdddialog) => {
    console.log(newAdddialog);
    if (!newAdddialog) {
      editedCheckMaterial.value = {
        employeeId: 0,
      };
    }
  });

  async function getCheckMaterials() {
    loadingStore.isLoading = true;
    try {
      const res = await CheckmaterialService.getCheckmaterials({});
      checkmaterials.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Check Material ได้");
    }
    loadingStore.isLoading = false;
  }

  async function findEmpCheckMaterial() {
    loadingStore.isLoading = true;
    try {
      const res = await employeeService.getEmployeesByTel(employee.value);
      employeeId.value = res.data.id;
      employeeName.value = res.data.name;
      console.log(employeeId);
      messageSuccessStore.showInfo("เจอข้อมูล Employee");

      console.log(employeeId.value);
      dialog.value = false;
      checkmaterialdetailStore.dialog = true;
      editedCheckMaterial.value.employeeId = employeeId.value;
    } catch (e) {
      messageStore.showError("ไม่เจอข้อมูล Employee");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function findEmpAddMaterial() {
    loadingStore.isLoading = true;
    try {
      const res = await employeeService.getEmployeesByTel(employee.value);
      employeeId.value = res.data.id;
      employeeName.value = res.data.name;
      messageSuccessStore.showInfo("เจอข้อมูล Employee");

      console.log(employeeId.value);
      Adddialog.value = false;
      billStore.Adddialog = true;
      editedCheckMaterial.value.employeeId = employeeId.value;
    } catch (e) {
      messageStore.showError("ไม่เจอข้อมูล Employee");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }
  function editCheckmaterial(checkmaterial: CheckMaterial) {
    editedCheckMaterial.value = JSON.parse(JSON.stringify(checkmaterial));
    dialog.value = true;
  }

  return {
    CheckmaterialService,
    getCheckMaterials,
    dialog,
    editedCheckMaterial,
    clearCheckMat,
    findEmpCheckMaterial,
    editCheckmaterial,
    employee,
    Adddialog,
    findEmpAddMaterial,
    Teldialog,
    // openCheckmaterial,
    clearCheckMatDetail,
    employeeId,
    employeeName,
    openCheckMaterial,
  };
});
