import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import { useAuthStore } from "./auth";
import { useMessageSuccessStore } from "./messageSuccess";
import type Bill from "@/types/Bill";
import type Material from "@/types/Material";
import billService from "@/services/bill";
import { useCheckMaterialStore } from "./checkmaterial";

export const useBillStore = defineStore("Bill", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const checkmaterialStore = useCheckMaterialStore();
  const authStore = useAuthStore();
  const Adddialog = ref(false);
  const bills = ref<Bill[]>([]);
  const editedBill = ref<Bill>({});
  const billStore = useBillStore();
  const messageSuccessStore = useMessageSuccessStore();
  const received = ref(0);
  const change = ref(0);
  const billdialog = ref(false);
  const BillList = ref<{ material: Material; amount: number; sum: number }[]>(
    []
  );

  async function openmaterialBill() {
    if (change.value < 0) {
      messageStore.showError("ยังไม่ได้รับเงินมาครบจำนวน");
    } else {
      billStore.billdialog = true;
    }
  }

  function clearBill() {
    BillList.value = [];

    received.value = 0;
  }
  function addMaterial(item: Material) {
    for (let i = 0; i < BillList.value.length; i++) {
      if (BillList.value[i].material.id === item.id) {
        BillList.value[i].amount++;
        BillList.value[i].sum = BillList.value[i].amount * item.price_per_unit;
        return;
      }
    }
    BillList.value.push({
      material: item,
      amount: 1,
      sum: 1 * item.price_per_unit,
    });
  }
  function deleteMaterialList(index: number) {
    BillList.value.splice(index, 1);
  }

  const sumPrice = computed(() => {
    let sum = 0;
    for (let i = 0; i < BillList.value.length; i++) {
      sum = sum + BillList.value[i].sum;
    }
    return sum;
  });

  async function openBill() {
    loadingStore.isLoading = true;
    const user: { id: number } = authStore.getUser();
    const billDetail = BillList.value.map(
      (item) =>
        <{ materialId: number; amount: number }>{
          materialId: item.material.id,
          amount: item.amount,
        }
    );
    const bill = {
      name: "ซื้อสินค้า",
      buy: received.value,
      billDetail: billDetail,
      employeeId: checkmaterialStore.employeeId,
    };
    console.log(bill);

    if (change.value < 0) {
      messageStore.showError("ยังไม่ได้รับเงินมาครบจำนวน");
      loadingStore.isLoading = false;
      return;
    }

    try {
      const res = await billService.saveBill(bill);
      Adddialog.value = false;
      messageSuccessStore.showInfo("การซื้อสำเร็จ");
      loadingStore.isLoading = false;
      // window.location.reload();
      clearBill();
    } catch (e) {
      messageStore.showError("ไม่สามารถซื้อสินค้าได้");
      console.log(e);
      loadingStore.isLoading = false;
    }
  }
  return {
    Adddialog,
    addMaterial,
    BillList,
    deleteMaterialList,
    sumPrice,
    received,
    change,
    openBill,
    clearBill,
    billdialog,
    openmaterialBill,
  };
});
