import { defineStore } from "pinia";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import type Checkmaterialdetail from "@/types/CheckmaterailDetail";
import { ref, watch } from "vue";
import checkmaterialdetailService from "@/services/checkmaterialdetail";
import { useMaterialStore } from "./material";
import { useCheckMaterialStore } from "./checkmaterial";

export const useCheckMaterialDetailStore = defineStore(
  "Checkmaterialdetail",
  () => {
    const loadingStore = useLoadingStore();
    const messageStore = useMessageStore();
    const materailStore = useMaterialStore();
    const usecheckMaterialStore = useCheckMaterialStore();
    const dialog = ref(false);
    const checkmaterialdetails = ref<Checkmaterialdetail[]>([]);
    const editedCheckmaterialDetail = ref<Checkmaterialdetail>({
      name: "",
      qty_last: 0,
      qty_remain: 0,
      qty_expire: 0,
      material: {
        name: "",
        min_quantity: 1,
        quantity: 1,
        unit: "",
        price_per_unit: 1,
        qty_remain: 0,
        qty_expire: 0,
      },
      checkmaterial: {
        employeeId: 1,
      },
    });

    watch(dialog, (newDialog, oldDialog) => {
      console.log(newDialog);
      if (!newDialog) {
        editedCheckmaterialDetail.value = {
          name: "",
          qty_last: 0,
          qty_remain: 0,
          qty_expire: 0,
          material: {
            name: "",
            min_quantity: 1,
            quantity: 1,
            unit: "",
            price_per_unit: 1,
            qty_remain: 0,
            qty_expire: 0,
          },
          checkmaterial: {
            employeeId: 1,
          },
        };
      }
    });
    async function getCheckmaterialDetails() {
      loadingStore.isLoading = true;
      try {
        const res = await checkmaterialdetailService.getCheckmaterialdetails(
          {}
        );
        checkmaterialdetails.value = res.data;
      } catch (e) {
        console.log(e);
        messageStore.showError("ไม่สามารถดึงข้อมูล Check Material ได้");
      }
      loadingStore.isLoading = false;
    }
    // async function saveCheckmaterialDetail() {
    //   loadingStore.isLoading = true;
    //   try {
    //     if (editedCheckmaterialDetail.value.id) {
    //       const res =
    //         await checkmaterialdetailService.updateCheckmaterialdetail(
    //           editedCheckmaterialDetail.value.id,
    //           editedCheckmaterialDetail.value
    //         );
    //     } else {
    //       editedCheckmaterialDetail.value = {
    //         name: editedCheckmaterialDetail.value.name,
    //         qty_last: materailStore.editedMaterial.quantity,
    //         qty_remain: editedCheckmaterialDetail.value.qty_remain,
    //         qty_expire: editedCheckmaterialDetail.value.qty_expire,
    //         material: {},
    //       };
    //       const res = await checkmaterialdetailService.saveCheckmaterialdetail(
    //         editedCheckmaterialDetail.value
    //       );
    //     }

    //     dialog.value = false;

    //     await getCheckmaterialDetails();
    //   } catch (e) {
    //     messageStore.showError("ไม่สามารถบันทึก Check Material ได้");
    //     console.log(e);
    //   }
    //   console.log(editedCheckmaterialDetail.value);
    //   loadingStore.isLoading = false;
    // }

    function editCheckmaterialdetail(checkmaterialdetail: Checkmaterialdetail) {
      editedCheckmaterialDetail.value = JSON.parse(
        JSON.stringify(checkmaterialdetail)
      );
      dialog.value = true;
    }

    return {
      editedCheckmaterialDetail,
      dialog,
      getCheckmaterialDetails,
      editCheckmaterialdetail,
    };
  }
);
