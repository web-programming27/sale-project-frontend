export default interface Store {
  id?: number;
  name: string;
  address_detail: string;
  address_sub_district: string;
  address_district: string;
  address_province: string;

  tel: string;

  createdAt?: Date;

  updatedAt?: Date;

  deletedAt?: Date;
}
