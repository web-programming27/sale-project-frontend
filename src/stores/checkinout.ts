import { defineStore } from "pinia";
import { useLoadingStore } from "./loading";
import { useAuthStore } from "./auth";
import chechinoutService from "@/services/checkinout";
import { useMessageStore } from "./message";
import { useMessageSuccessStore } from "./messageSuccess";
import { ref } from "vue";
import router from "@/router";
export const useCheckinoutStore = defineStore("Checkinout", () => {
  const loadingStore = useLoadingStore();
  const authStore = useAuthStore();
  const messageStore = useMessageStore();
  const messageSuccessStore = useMessageSuccessStore();
  const checkinoutId = ref(0);
  const checkinDialog = ref(false);
  const checkoutDialog = ref(false);

  async function openCheckin() {
    try {
      loadingStore.isLoading = true;
      const user: { id: number } = authStore.getUser();
      const checkinout = {
        employeeId: user.id,
      };
      const res = await chechinoutService.saveCheckinouts(checkinout);
      checkinoutId.value = res.data.id;
      messageSuccessStore.showInfo("Check In สำเร็จ");
      console.log("checkinoutId", checkinoutId);
      console.log("user", user);
      console.log("employeeId", checkinout);
      console.log("res", res);
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถ Check In ได้");
    }
    loadingStore.isLoading = false;
  }

  async function openCheckout() {
    loadingStore.isLoading = true;
    try {
      const res = await chechinoutService.updateCheckinout(checkinoutId.value);
      messageSuccessStore.showInfo("Check Out สำเร็จ");
      console.log(res.data);
      console.log(checkinoutId.value);
    } catch (e) {
      console.log("เงาะ", e);
      messageStore.showError("ไม่สามารถ Check Out ได้");
    }
    loadingStore.isLoading = false;
    router.replace("/");
  }
  return {
    openCheckin,
    openCheckout,
    checkoutDialog,
    checkinDialog,
    checkinoutId,
  };
});
