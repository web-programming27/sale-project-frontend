import type Checkmaterialdetail from "@/types/CheckmaterailDetail";
import http from "./axios";
function getCheckmaterialdetails(params: any) {
  return http.get("/checkmaterialdetails", { params: params });
}
function saveCheckmaterialdetail(checkmaterials: Checkmaterialdetail) {
  return http.post("/checkmaterialdetails", checkmaterials);
}
function updateCheckmaterialdetail(
  id: number,
  checkmaterials: Checkmaterialdetail
) {
  return http.patch(`/checkmaterialdetails/${id}`, checkmaterials);
}
function daleteCheckmaterialdetail(id: number) {
  return http.delete(`/checkmaterialdetails/${id}`);
}
export default {
  getCheckmaterialdetails,
  saveCheckmaterialdetail,
  updateCheckmaterialdetail,
  daleteCheckmaterialdetail,
};
