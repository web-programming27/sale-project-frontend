import type User from "./User";

export default interface SummarySalary {
  id?: number;

  ss_date: Date;

  ss_work_hour: number;

  ss_salary: number;

  employee: User;

  createdAt?: Date;

  updatedAt?: Date;

  deletedAt?: Date;
}
