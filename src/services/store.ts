import type Store from "@/types/Store";
import http from "./axios";
function getStores(params: any) {
  return http.get("/stores", { params: params });
}
function saveStore(stores: Store) {
  return http.post("/stores", stores);
}
function updateStore(id: number, stores: Store) {
  return http.patch(`/stores/${id}`, stores);
}
function daleteStore(id: number) {
  return http.delete(`/stores/${id}`);
}
export default { getStores, saveStore, updateStore, daleteStore };
