import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Customer from "@/types/Customer";
import { useLoadingStore } from "./loading";
import customerService from "@/services/customer";
import { useMessageStore } from "./message";

export const useCustomerPageStore = defineStore("customerPage", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const page = ref(1);
  const take = ref(10);
  const keyword = ref("");
  const customers = ref<Customer[]>([]);
  const lastPage = ref(0);
  watch(keyword, async (newKeyword, oldKeyword) => {
    await getCustomers();
  });
  watch(page, async (newPage, oldPage) => {
    await getCustomers();
  });
  watch(lastPage, async (newlastPage, oldlastPage) => {
    if (newlastPage < page.value) {
      page.value = 1;
    }
  });
  async function getCustomers() {
    loadingStore.isLoading = true;
    try {
      const res = await customerService.getCustomers({
        page: page.value,
        take: take.value,
        keyword: keyword.value,
      });
      customers.value = res.data.data;
      lastPage.value = res.data.lastPage;
      console.log(customers.value);
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Customer ได้");
    }
    loadingStore.isLoading = false;
  }

  return { page, take, keyword, customers, getCustomers, lastPage };
});
