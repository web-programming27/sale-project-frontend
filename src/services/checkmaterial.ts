import type Checkmaterial from "@/types/CheckMaterial";
import http from "./axios";
function getCheckmaterials(params: any) {
  return http.get("/checkmaterials", { params: params });
}
function saveCheckmaterial(checkmaterials: Checkmaterial) {
  return http.post("/", checkmaterials);
}
function saveCheckMaterial(checkmaterials: {
  employeeId: number;
  checkmaterialDetails: {
    materialId: number;
    qty_remain: number;
    qty_expire: number;
  }[];
}) {
  return http.post("/checkmaterials", checkmaterials);
}

function updateCheckmaterial(id: number, checkmaterials: Checkmaterial) {
  return http.patch(`/checkmaterials/${id}`, checkmaterials);
}
function daleteCheckmaterial(id: number) {
  return http.delete(`/checkmaterials/${id}`);
}
export default {
  getCheckmaterials,
  saveCheckmaterial,
  updateCheckmaterial,
  daleteCheckmaterial,
  saveCheckMaterial,
};
