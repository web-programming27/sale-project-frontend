import type Catagory from "@/types/Catagory";
import http from "./axios";
function getCatagorys() {
  return http.get("/catagorys");
}
function saveCatagory(catagorys: Catagory) {
  return http.post("/catagorys", catagorys);
}
function updateCatagory(id: number, catagorys: Catagory) {
  return http.patch(`/catagorys/${id}`, catagorys);
}
function deleteCatagory(id: number) {
  return http.delete(`/catagorys/${id}`);
}
export default { getCatagorys, saveCatagory, updateCatagory, deleteCatagory };
