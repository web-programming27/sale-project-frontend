import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type Store from "@/types/Store";
import storeService from "@/services/store";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useStoreStore = defineStore("Store", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const stores = ref<Store[]>([]);
  const editedStore = ref<Store>({
    name: "",
    address_detail: "",
    address_sub_district: "",
    address_district: "",
    address_province: "",
    tel: "",
  });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedStore.value = {
        name: "",
        address_detail: "",
        address_sub_district: "",
        address_district: "",
        address_province: "",
        tel: "",
      };
    }
  });
  async function getStores() {
    loadingStore.isLoading = true;
    try {
      const res = await storeService.getStores({});
      stores.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Store ได้");
    }
    loadingStore.isLoading = false;
  }
  async function saveStore() {
    loadingStore.isLoading = true;
    try {
      if (editedStore.value.id) {
        const res = await storeService.updateStore(
          editedStore.value.id,
          editedStore.value
        );
      } else {
        const res = await storeService.saveStore(editedStore.value);
      }

      dialog.value = false;

      await getStores();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Store ได้");
      console.log(e);
    }
    window.location.reload();
    loadingStore.isLoading = false;
  }
  async function deleteStore(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await storeService.daleteStore(id);
      await getStores();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Store ได้");
    }
    window.location.reload();
    loadingStore.isLoading = false;
  }

  function editStore(store: Store) {
    editedStore.value = JSON.parse(JSON.stringify(store));
    dialog.value = true;
  }
  return {
    stores,
    getStores,
    dialog,
    editedStore,
    saveStore,
    editStore,
    deleteStore,
  };
});
