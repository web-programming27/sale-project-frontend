import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Material from "@/types/Material";
import { useLoadingStore } from "./loading";
import materialService from "@/services/material";
import { useMessageStore } from "./message";

export const useMaterialPageStore = defineStore("materialPage", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const page = ref(1);
  const take = ref(10);
  const keyword = ref("");
  const materials = ref<Material[]>([]);

  const lastPage = ref(0);
  watch(keyword, async (newKeyword, oldKeyword) => {
    await getMaterials();
  });
  watch(page, async (newPage, oldPage) => {
    await getMaterials();
  });
  watch(lastPage, async (newlastPage, oldlastPage) => {
    if (newlastPage < page.value) {
      page.value = 1;
    }
  });
  async function getMaterials() {
    loadingStore.isLoading = true;
    try {
      const res = await materialService.getMaterials({
        page: page.value,
        take: take.value,
        keyword: keyword.value,
      });
      materials.value = res.data.data;
      lastPage.value = res.data.lastPage;
      console.log(materials.value);
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Employee ได้");
    }
    loadingStore.isLoading = false;
  }

  return { page, take, keyword, materials, getMaterials, lastPage };
});
