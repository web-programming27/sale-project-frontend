export default interface SaleByTime {
  time_of_day: string;
  total_sales: number;
}
