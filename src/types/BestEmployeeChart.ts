export default interface BestEmployeeChart {
  id: number;
  name: string;
  surname: string;
  position: string;
  top_emp: number;
}
