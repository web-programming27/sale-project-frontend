import type Bill from "@/types/Bill";
import http from "./axios";
function getBills() {
  return http.get("/bills");
}
function saveBill(bills: {
  name: string;
  buy: number;
  billDetail: { materialId: number; amount: number }[];
  employeeId: number;
}) {
  return http.post("/bills", bills);
}
function updateBill(id: number, bills: Bill) {
  return http.patch(`/bills/${id}`, bills);
}
function deleteBill(id: number) {
  return http.delete(`/bills/${id}`);
}
export default { getBills, saveBill, updateBill, deleteBill };
