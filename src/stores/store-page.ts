import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Store from "@/types/Store";
import { useLoadingStore } from "./loading";
import storeService from "@/services/store";
import { useMessageStore } from "./message";

export const useStorePageStore = defineStore("storePage", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const page = ref(1);
  const take = ref(10);
  const keyword = ref("");
  const stores = ref<Store[]>([]);
  const lastPage = ref(0);
  watch(keyword, async (newKeyword, oldKeyword) => {
    await getStores();
  });
  watch(page, async (newPage, oldPage) => {
    await getStores();
  });
  watch(lastPage, async (newlastPage, oldlastPage) => {
    if (newlastPage < page.value) {
      page.value = 1;
    }
  });
  async function getStores() {
    loadingStore.isLoading = true;
    try {
      const res = await storeService.getStores({
        page: page.value,
        take: take.value,
        keyword: keyword.value,
      });
      stores.value = res.data.data;
      lastPage.value = res.data.lastPage;
      console.log(stores.value);
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Store ได้");
    }
    loadingStore.isLoading = false;
  }

  return { page, take, keyword, stores, getStores, lastPage };
});
