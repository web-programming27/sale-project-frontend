import type Product from "./Product";
import type Reciept from "./Reciept";

export default interface RecieptDetail {
  id?: number;

  name?: string;

  amount?: number;

  price?: number;

  total?: number;

  product?: Product;

  reciept?: Reciept;

  createdDate?: Date;

  updatedDate?: Date;

  deletedDate?: Date;
}
