import type BillDetail from "./BillDetail";

export default interface Bill {
  id?: number;
  name?: string;
  amount?: number;
  totol?: number;
  buy?: number;
  change?: number;
  createdDate?: Date;
  updatedDate?: Date;

  deletedDate?: Date;
  billDetail?: BillDetail[];
}
