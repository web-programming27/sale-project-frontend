import { ref, watch } from "vue";
import { defineStore } from "pinia";
import reportService from "@/services/report";
import employeeService from "@/services/employee";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import type User from "@/types/User";
import type BestEmployeeChart from "@/types/BestEmployeeChart";

export const useEmployeeStore = defineStore("Employee", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const employees = ref<User[]>([]);
  const employeeBySales = ref<BestEmployeeChart[]>([]);
  const name = ref<string[]>([]);
  const surname = ref<string[]>([]);
  const topemp = ref<number[]>([]);
  const editedEmployee = ref<User>({
    name: "",
    login: "",
    password: "",
    address: "",
    tel: "",
    email: "",
    position: "",
    hourlywage: 0,
  });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedEmployee.value = {
        name: "",
        login: "",
        password: "",
        address: "",
        tel: "",
        email: "",
        position: "",
        hourlywage: 0,
      };
    }
  });
  async function getemployeeBySales() {
    try {
      const res = await reportService.getBestEmployee();
      console.log("response from API", res);
      employeeBySales.value = res.data[0];
      for (const item of res.data) {
        name.value.push(item.name);
        surname.value.push(item.surname);
        topemp.value.push(item.top_emp);
      }
      console.log("name.data", name.value);
      console.log("surname.data", surname.value);
      console.log("topemp.data", topemp.value);
      console.log("employeeBySales.value", employeeBySales.value);
      console.log("res.data", res.data);
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่มีข้อมูล Employee");
    }
  }

  // async function getemployeeBySales() {
  //   try {
  //     const res = await reportService.getBestEmployee();
  //     employeeBySales.value = res.data;
  //     console.log("employeeBySales.value", employeeBySales.value);
  //     console.log("res.data", res.data);
  //   } catch (e) {
  //     console.log(e);
  //     messageStore.showError("ไม่มีข้อมูล Employee");
  //   }
  // }

  async function getEmployees() {
    loadingStore.isLoading = true;
    try {
      const res = await employeeService.getEmployees({});
      employees.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Employee ได้");
    }
    loadingStore.isLoading = false;
  }
  async function saveEmployee() {
    loadingStore.isLoading = true;
    try {
      if (editedEmployee.value.id) {
        const res = await employeeService.updateEmployee(
          editedEmployee.value.id,
          editedEmployee.value
        );
      } else {
        const res = await employeeService.saveEmployee(editedEmployee.value);
      }

      dialog.value = false;

      await getEmployees();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Employee ได้");
      console.log(e);
    }
    window.location.reload();
    loadingStore.isLoading = false;
  }
  async function deleteEmployee(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await employeeService.daleteEmployee(id);
      await getEmployees();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Employee ได้");
    }
    loadingStore.isLoading = false;
    window.location.reload();
  }

  function editEmployee(employee: User) {
    editedEmployee.value = JSON.parse(JSON.stringify(employee));
    dialog.value = true;
  }
  return {
    employees,
    getEmployees,
    dialog,
    editedEmployee,
    saveEmployee,
    editEmployee,
    deleteEmployee,
    getemployeeBySales,
    employeeBySales,
  };
});
