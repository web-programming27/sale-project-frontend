import type SummarySalary from "@/types/SummarySalary";
import http from "./axios";
function getSummarysalary() {
  return http.get("/summarysalary");
}
function saveSummarysalary(saveSummarysalary: SummarySalary) {
  return http.post("/summarysalary", saveSummarysalary);
}
function updateSummarysalary(id: number, summarysalary: SummarySalary) {
  return http.patch(`/summarysalary/${id}`, summarysalary);
}
function daleteSummarysalary(id: number) {
  return http.delete(`/summarysalary/${id}`);
}
export default {
  getSummarysalary,
  saveSummarysalary,
  updateSummarysalary,
  daleteSummarysalary,
};
