import { ref } from "vue";
import { defineStore } from "pinia";
export enum DialogType {
  info,
  error,
  confirm,
}
export const useMessageSuccessStore = defineStore("messageSuccess", () => {
  const isShow = ref(false);
  const messageSuccess = ref("");
  const type = ref<DialogType>(DialogType.info);

  function showError(text: string) {
    type.value = DialogType.error;
    messageSuccess.value = text;
    isShow.value = true;
  }

  function showInfo(text: string) {
    type.value = DialogType.info;
    messageSuccess.value = text;
    isShow.value = true;
  }
  function showConfirm(text: string) {
    type.value = DialogType.confirm;
    messageSuccess.value = text;
    isShow.value = true;
  }

  return { isShow, messageSuccess, showError, showInfo, showConfirm };
});
