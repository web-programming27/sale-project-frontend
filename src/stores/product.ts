import { computed, ref, watch } from "vue";
import { defineStore } from "pinia";
import type Product from "@/types/Product";
import productService from "@/services/product";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useProductStore = defineStore("Product", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const catagory = ref(3);
  const orderList = ref<
    { product: Product; amount: number; sum: number; image?: string }[]
  >([]);

  function addProduct(item: Product) {
    for (let i = 0; i < orderList.value.length; i++) {
      if (orderList.value[i].product.id === item.id) {
        orderList.value[i].amount++;
        orderList.value[i].sum = orderList.value[i].amount * item.price;
        orderList.value[i].image = item.image;
        return;
      }
    }
    orderList.value.push({
      product: item,
      amount: 1,
      sum: 1 * item.price,
      image: item.image,
    });
  }

  function deleteProductList(index: number) {
    orderList.value.splice(index, 1);
  }
  const sumPrice = computed(() => {
    let sum = 0;
    for (let i = 0; i < orderList.value.length; i++) {
      sum = sum + orderList.value[i].sum;
    }
    return sum;
  });

  const taxPrice = computed(() => {
    const tax = 0.1 * sumPrice.value;
    return tax;
  });

  const totalPrice = computed(() => {
    const total = taxPrice.value + sumPrice.value;
    return total;
  });

  const products = ref<Product[]>([]);
  const editedProduct = ref<Product & { files: File[] }>({
    name: "",
    catagoryId: 2,
    size: 0,
    size_unit: "",
    price: 0,
    qty: 0,
    image: "no_image.jpg",
    files: [],
  });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedProduct.value = {
        name: "",
        catagoryId: 2,
        size: 0,
        size_unit: "",
        price: 0,
        qty: 0,
        image: "no_image.jpg",
        files: [],
      };
    }
  });
  watch(catagory, async (newCatagory, oldCatagory) => {
    await getProductsByCatagory(newCatagory);
  });
  async function getProductsByCatagory(catagory: number) {
    loadingStore.isLoading = true;
    try {
      const res = await productService.getProductsByCatagory(catagory);
      products.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Product ได้");
    }
    loadingStore.isLoading = false;
  }

  async function getProducts() {
    loadingStore.isLoading = true;
    try {
      const res = await productService.getProducts();
      products.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Product ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveProduct() {
    loadingStore.isLoading = true;
    try {
      if (editedProduct.value.id) {
        const res = await productService.updateProduct(
          editedProduct.value.id,
          editedProduct.value
        );
      } else {
        const res = await productService.saveProduct(editedProduct.value);
      }

      dialog.value = false;

      await getProducts();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Product ได้");
      console.log(e);
    }
    window.location.reload();
    loadingStore.isLoading = false;
  }
  async function deleteProduct(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await productService.daleteProduct(id);
      await getProducts();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Product ได้");
    }
    window.location.reload();
    loadingStore.isLoading = false;
  }

  function editProduct(product: Product) {
    editedProduct.value = JSON.parse(JSON.stringify(product));
    dialog.value = true;
  }
  return {
    products,
    getProducts,
    dialog,
    editedProduct,
    saveProduct,
    editProduct,
    deleteProduct,
    addProduct,
    deleteProductList,
    sumPrice,
    orderList,
    taxPrice,
    totalPrice,
    catagory,
    getProductsByCatagory,
  };
});
