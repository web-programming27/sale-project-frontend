import type Reciept from "@/types/Reciept";
import http from "./axios";
function getReciepts() {
  return http.get("/reciepts");
}
function saveReciept(reciepts: {
  customerId: number;
  storeId: number;
  employeeId: number;
  discount: number;
  received: number;
  recieptDetails: { productId: number; amount: number }[];
  payment: string;
}) {
  return http.post("/reciepts", reciepts);
}
function updateReciept(id: number, reciepts: Reciept) {
  return http.patch(`/reciepts/${id}`, reciepts);
}
function deleteReciept(id: number) {
  return http.delete(`/reciepts/${id}`);
}
export default { getReciepts, saveReciept, updateReciept, deleteReciept };
