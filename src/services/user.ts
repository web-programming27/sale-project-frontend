import type User from "@/types/User";
import http from "./axios";
function getUsers(params: any) {
  return http.get("/users", { params: params });
}
function saveUser(users: User) {
  return http.post("/users", users);
}
function updateUser(id: number, users: User) {
  return http.patch(`/users/${id}`, users);
}
function deleteUser(id: number) {
  return http.delete(`/users/${id}`);
}
export default { getUsers, saveUser, updateUser, deleteUser };
